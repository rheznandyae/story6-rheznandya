from django.shortcuts import render, redirect
from .models import Orang , Kegiatan
from .forms import FormKegiatan , FormPeserta


def home_view(request):

    obj = Kegiatan.objects.all()
    
    response = {
        "kegiatan" : obj,
    }

    return render(request, "home.html", response)

def tambah_peserta_view(request, *args, **kwargs):
    form = FormPeserta()

    if request.method == "POST":
        namaPeserta = request.POST.get("nama")
        pesertaBaru = Orang.objects.create(nama = namaPeserta)
        kegiatan = Kegiatan.objects.get(id = kwargs["id"])
        kegiatan.peserta.add(pesertaBaru)
        return redirect("/")
    
    
    response = {
        "form" : form,
    }

    return render(request, "tambah-peserta.html", response )

def tambah_kegiatan_view(request):

    form = FormKegiatan()
    if request.method == "POST":
        form = FormKegiatan(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")
    response = {
        "form" : form,
    }
    return render(request, "tambah-kegiatan.html", response)
