from django.test import TestCase
from .models import Kegiatan, Orang



class test_project(TestCase):
    def test_root_url(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_url_kegiatan(self):
        response = self.client.get("/tambah-kegiatan/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tambah-kegiatan.html')

    def test_url_tambah_peserta(self):
        
        kegiatantest = Kegiatan.objects.create(nama_kegiatan = "test")
        response = self.client.get(kegiatantest.absolute_url())
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tambah-peserta.html')
        self.assertIn("Tambahkan Peserta",html_response)
    

class Test_database(TestCase):

    def tambah_kegiatan(self):
        response = self.client.post("/tambah-kegiatan/", data = {'nama_kegiatan' : "test_post_kegiatan"})
        countData = Kegiatan.objects.all().count()
        self.assertEqual(countData,1)

    def tambah_peserta(self):
        kegiatantest = Kegiatan.objects.create(nama_kegiatan = "test")
        url_tambah = kegiatantest.absolute_url()
        response = self.client.post(url_tambah, data = {'nama' : "test_post_peserta"})
        countData = kegiatantest.peserta.objects.all().count()
        self.assertEqual(countData,1)











        