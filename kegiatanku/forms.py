from django import forms
from .models import Orang , Kegiatan

class FormPeserta(forms.ModelForm):
    class Meta:
        model = Orang
        fields = [
            'nama',
        ]

        widgets = {
            'nama' : forms.TextInput(attrs={'class' : 'form-control'}),
        }


class FormKegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'nama_kegiatan',
        ]

        widgets = {
            'nama_kegiatan' : forms.TextInput(attrs={'class' : 'form-control'}),
        }