from django.db import models
from django.urls import reverse


class Orang(models.Model):
    nama = models.CharField(max_length = 100)
    class Meta:
        ordering = ['nama']

    def __str__(self):
        return self.nama

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 100)
    peserta = models.ManyToManyField(Orang)

    class Meta:
        ordering = ['nama_kegiatan']

    def absolute_url(self):
        return reverse("kegiatanku:tambah-peserta", kwargs = {"id" : self.id})
        
    def __str__(self):
        return self.nama_kegiatan

