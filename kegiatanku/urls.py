from django.urls import path
from .views import home_view, tambah_peserta_view, tambah_kegiatan_view

app_name = 'kegiatanku'

urlpatterns = [
    path('', home_view, name= "home"),
    path('tambah-kegiatan/', tambah_kegiatan_view, name = "tambah-kegiatan"),
    path('tambah-peserta/<int:id>', tambah_peserta_view, name = "tambah-peserta"),
]