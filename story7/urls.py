from django.urls import path
from .views import story7_views


app_name = 'story7'

urlpatterns = [
    path('', story7_views, name= "story7"),
]