$(".toggle").click(function () { 
  $(this).parent().next().slideToggle();
});

$(".up").click(function () { 
  const curr = $(this).parent().parent().parent()
  const currClass = 'accor-' + curr.css('order')
  const prevClass = 'accor-' + parseInt(parseInt(curr.css('order'))  - 1)

  if(parseInt(curr.css('order')) > 1){
    $("." + prevClass).addClass(currClass)
    $("." + prevClass).removeClass(prevClass)
    curr.removeClass(currClass)
    curr.addClass(prevClass)
  }
});

$(".down").click(function () { 
  const curr = $(this).parent().parent().parent()
  const currClass = 'accor-' + curr.css('order')
  const nextClass = 'accor-' + parseInt(parseInt(curr.css('order')) + 1)  

  if(parseInt(curr.css('order')) < 4){
    $("." + nextClass).addClass(currClass)
    $("." + nextClass).removeClass(nextClass)
    curr.removeClass(currClass)
    curr.addClass(nextClass)
  }
});
